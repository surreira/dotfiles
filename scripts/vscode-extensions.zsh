#!/bin/zsh

source $HOME/.aliases

# WEBDEV VSCODE PROFILE
webProfileExtensions=(
  arcticicestudio.nord-visual-studio-code
  bradlc.vscode-tailwindcss
  dbaeumer.vscode-eslint
  esbenp.prettier-vscode
  formulahendry.auto-close-tag
  formulahendry.auto-rename-tag
  GitHub.github-vscode-theme
  PKief.material-icon-theme
  shd101wyy.markdown-preview-enhanced
  streetsidesoftware.code-spell-checker
  streetsidesoftware.code-spell-checker-portuguese
  usernamehw.errorlens
  VisualStudioExptTeam.vscodeintellicode
  mhutchie.git-graph
  ms-vscode-remote.remote-ssh
)

for i in ${webProfileExtensions[@]}; do
  webdev --install-extension $i
done

# PYTHONDEV VSCODE PROFILE
pythonProfileExtensions=(
  arcticicestudio.nord-visual-studio-code
  formulahendry.auto-close-tag
  formulahendry.auto-rename-tag
  GitHub.github-vscode-theme
  PKief.material-icon-theme
  shd101wyy.markdown-preview-enhanced
  streetsidesoftware.code-spell-checker
  streetsidesoftware.code-spell-checker-portuguese
  usernamehw.errorlens
  VisualStudioExptTeam.vscodeintellicode
  ms-python.black-formatter
  ms-python.python
  ms-python.vscode-pylance
  mhutchie.git-graph
  ms-vscode-remote.remote-ssh
)

for i in ${pythonProfileExtensions[@]}; do
  pythondev --install-extension $i
done

# WORK VSCODE PROFILE
workProfileExtensions=(
  arcticicestudio.nord-visual-studio-code
  esbenp.prettier-vscode
  akamud.vscode-theme-onelight
  PKief.material-icon-theme
  shd101wyy.markdown-preview-enhanced
  streetsidesoftware.code-spell-checker
  streetsidesoftware.code-spell-checker-portuguese
  usernamehw.errorlens
  VisualStudioExptTeam.vscodeintellicode
  mhutchie.git-graph
  ms-vscode-remote.remote-ssh
)

for i in ${workProfileExtensions[@]}; do
  work --install-extension $i
done
